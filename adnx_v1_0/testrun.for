C
C  TEST PROGRAM FOR EVULUATING GENERALIZED FERMI-DIRAC FUNCTION
C  OR ITS DERIVATIVES (UP TO 3RD ORDER).
C
C  REFERENFE:
C      Gong Z.G., Zejda L., Dappen W., Aparicio J.M., Comp. Phys. Commun.
C               in preparation, 2000.
C      Aparicio J.M., ApJS, 117, 627-632, 1998.
C
      PROGRAM GFD_D3_TESTRUN
C
      DOUBLE PRECISION GFD_D3,DK,ETA,ETAMAX,ETAMIN,ETASTEP,
     .       BETA,BETAMAX,BETAMIN,BETASTEP,TMP
      INTEGER IB
C
      PRINT *,"Input index value k"
      READ(5,*) DK
      WRITE(6,101) DK
      PRINT *,"Input which derivative to be evaluated"
      READ(5,*) IB
      WRITE(6,102) IB
      PRINT *,"Input eta (min, max, step)"
      READ(5,*) ETAMIN,ETAMAX,ETASTEP
      WRITE(6,103) ETAMIN,ETAMAX,ETASTEP
      PRINT *,"Input beta (min, max, step)"
      READ(5,*) BETAMIN,BETAMAX,BETASTEP
      WRITE(6,103) BETAMIN,BETAMAX,BETASTEP
      WRITE(6,*)
C
      DO ETA=ETAMIN,ETAMAX,ETASTEP
        DO BETA=BETAMIN,BETAMAX,BETASTEP
          TMP=GFD_D3(IB,DK,ETA,BETA)
          WRITE(6,100) DK,ETA,BETA,IB,TMP
        END DO
      END DO
C
 100  FORMAT(1X,"K=",F4.1,3X,"ETA=",F7.2,3X,"BETA=",F7.2,/,
     .       2X,"FD(",I1,") =",1PE14.6)
 101  FORMAT(1X,"K= ",F4.1)
 102  FORMAT(1X,"IB=",I3)
 103  FORMAT(3(2X,F7.2))
      STOP
      END
