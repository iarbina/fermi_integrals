program main
  use,intrinsic::iso_fortran_env, wp=>real64
  use omp_lib
  use constants
  use lepeos_mod
  implicit none
  real(wp), allocatable :: mue(:)
  real(wp) :: mue_min, mue_max, dmue
  real(wp) :: ntot, npar, nant
  real(wp) :: ptot, ppar, pant
  real(wp) :: utot, upar, uant
  real(wp) :: stot, spar, sant
  real(wp) :: rho, temp, cheme, nb, ye_in
  integer  :: i, np, iounit

  print'(":: Electron number calculator for arbitrary degenerate and relativistic gas")'
  print'(":: Enter temperature in MeV:")'
  read(input_unit,*) temp

  np = 100000
  mue_min =-1.0e5_wp
  mue_max = 1.0e5_wp

  dmue = (mue_max - mue_min) / (np - 1)

  mue = [ (mue_min + (i-1) * dmue, i=1, np) ]

  print'(":: OMP_MAX_THREADS() ",g0)', OMP_GET_MAX_THREADS()
  open(newunit=iounit, file='eleceos.dat')
  !$OMP PARALLEL DO NUM_THREADS(OMP_GET_MAX_THREADS()) &
  !$OMP PRIVATE(ntot, npar, nant, ptot, ppar, pant, utot, upar, uant, spar, sant)
  do i=1, np
     !print'(" Number threads: ",g0," thread number ",g0)', OMP_GET_NUM_THREADS(), OMP_GET_THREAD_NUM()
     call abundlep (mlepmev, temp, mue(i), ntot, npar, nant)
     call preslep  (mlepmev, temp, mue(i), ptot, ppar, pant)
     call enerlep  (mlepmev, temp, mue(i), utot, upar, uant)
     !spar = entrolep (temp,  mue(i), npar, ppar, upar)
     !sant = entrolep (temp, -mue(i), nant, pant, uant)
     stot = (ptot + utot + mue(i) * ntot) / temp
     write(iounit,*) nb, mue(i), ntot, ptot, utot, stot
  end do
  !$OMP END PARALLEL DO
  close(iounit)
end program main
