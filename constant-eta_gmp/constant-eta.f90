module fermi
  interface
     subroutine dfermi(dk,eta,theta,fd,fdeta,fdtheta)
       implicit none
       real(8), intent(in)  :: dk,eta,theta
       real(8), intent(out) :: fd,fdeta,fdtheta
     end subroutine dfermi
  end interface
end module fermi

program etaconst
  use fermi
  implicit none
  real(8), parameter :: mu = 1.66053906660d-24 ! g
  real(8), parameter :: memev   = 0.51099891d0 ! MeV
  real(8), parameter :: muonmev   = 105.6583755d0 ! MeV  
  real(8), parameter :: hbarc =  1.973269804d-11 ! MeV cm
  real(8), parameter :: kb = 8.617333262d-11 ! MeV / K 
  real(8), parameter :: pi = 3.14159265358979323846d0

  
  real(8) :: f12, f12eta, f32, f32eta, xxx, dummy
  real(8) :: eta, etap, beta, factor
  real(8) :: lt, temp, rhoye
  

  factor = 2.0d0*mu*memev*memev*memev
!  factor = 2.0d0*mu*muonmev*muonmev*muonmev
  factor = factor/(sqrt(2.0d0)*pi*pi*hbarc*hbarc*hbarc)

  eta = -3.0d0

  lt = -1.0d0
  do while (lt <= 12.0d0)
     temp = 10**(lt)
!     eta = - memev/(kb*temp)
     beta = (kb*temp)/memev
     !     beta = (kb*temp)/muonmev
     call dfermi(0.5d0, eta, beta, f12, f12eta, dummy)
     call dfermi(1.5d0, eta, beta, f32, f32eta, dummy)
     xxx = f12 + beta*f32
     etap = - eta - 2.0d0/beta
     call dfermi(0.5d0, etap, beta, f12, f12eta, dummy)
     call dfermi(1.5d0, etap, beta, f32, f32eta, dummy)
!     xxx = xxx - (f12 + beta*f32)
     dummy = f12 + beta*f32
     xxx = xxx - dummy 
     rhoye = factor*xxx*beta**(3.0d0/2.0d0)
     write (*,'(2es14.6,f6.2)') rhoye, temp,log10(temp)
     lt = lt + 0.5d0
  end do
end program etaconst
