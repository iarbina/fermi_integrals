module constants
  implicit none
  !  
  ! mathematical and physical constants (in cgs,except e0 which is in ev)
  ! 
  ! Values from the Particle Data Group constants, based on the
  ! "CODATA Recommended Values of the Fundamental Physical Constants:
  ! 2002", http://physics.nist.gov/constants
  
  integer, private, parameter :: wp = kind(1.0d0)
    
  real(wp), parameter :: pi = 3.14159265358979323846d0
  real(wp), parameter :: a2rad = pi/180.0d0
  real(wp), parameter :: rad2a = 180.0d0/pi
  real(wp), parameter :: eulercon = 0.57721566490153286061d0
  real(wp), parameter :: zeta3 = 1.20205690315959428540d0
  real(wp), parameter :: zeta5 = 1.03692775514336992633d0
  
  real(wp), parameter :: g = 6.67428d-8 ! cm^3 / g s^2
  real(wp), parameter :: h = 6.62606896d-27 ! erg s
  real(wp), parameter :: hbar = 1.054571628d-27 ! erg s
  real(wp), parameter :: hbarc = 3.1615262053d-17 ! erg cm
  real(wp), parameter :: hbarcmev = 1.973269788d-11 ! MeV cm
  real(wp), parameter :: hbarcfm = 197.3269788d0 ! MeV fm
  real(wp), parameter :: fine = 7.2973525376d-3 
  real(wp), parameter :: clight = 2.99792458d10 ! cm/s
  real(wp), parameter :: qe = sqrt(fine*hbarc)
  real(wp), parameter :: avo = 6.02214179d23
  real(wp), parameter :: kerg = 1.3806504d-16 ! erg / K
  real(wp), parameter :: kev = 8.617343d-5 ! eV / K
  real(wp), parameter :: GKMeV = 8.617343d-2 ! MeV / GK
  real(wp), parameter :: amu = 1.660538782d-24 ! g
  real(wp), parameter :: mn = 1.674927211d-24 ! g
  real(wp), parameter :: mp = 1.672621637d-24 ! g
  real(wp), parameter :: me = 9.10938215d-28 ! g
  real(wp), parameter :: mm = 1.883531627d-25 ! g
  real(wp), parameter :: mnMeV = 939.565346d0 ! MeV
  real(wp), parameter :: mpMeV = 938.272013d0 ! MeV
  real(wp), parameter :: meMeV = 0.510998910d0 ! MeV
  real(wp), parameter :: mmuMeV = 105.6583668d0 ! MeV
  real(wp), parameter :: mpiMeV = 139.57061d0 ! MeV
  real(wp), parameter :: rbohr = 5.2917720859e-9 ! cm
  real(wp), parameter :: hion = 13.60569193d0 ! eV Rydberg * h c
  real(wp), parameter :: ev2erg = 1.602176487d-12
  real(wp), parameter :: mev2erg = 1.602176487d-6
  real(wp), parameter :: ssol = 5.670400d-5 ! erg / s K^4 cm^2
  real(wp), parameter :: asol = 4.0d0 * ssol / clight
  real(wp), parameter :: weinlam = h*clight/(kerg * 4.965114232d0)
  real(wp), parameter :: weinfre = 2.821439372d0*kerg/h
  real(wp), parameter :: rhonuc  = 2.342d14 ! g / cm^3
  real(wp), parameter :: msol    = 1.9885d33 ! g 
  real(wp), parameter :: rsol    = 6.9551d10 ! cm
  real(wp), parameter :: lsol    = 3.828d33 ! erg / s 
  real(wp), parameter :: mearth  = 5.9726d27 ! g
  real(wp), parameter :: rearth  = 6.378137d8 ! cm
  real(wp), parameter :: pc = 3.0856775854d18 ! cm
  real(wp), parameter :: ly = 9.46073047258d17 ! cm
  real(wp), parameter :: au = 1.49597870691d13 ! cm 
  real(wp), parameter :: secyer  = 3.15576d7 ! s
end module constants
