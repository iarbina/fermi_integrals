#!/usr/bin/env python3

import re
import sys
import numpy as np
import matplotlib.pyplot as plt

# regex pattern for scientific notation floats
scinot = '[+-]?\d\.\d+\w[+-]\d+'
flonot = '[+-]?\d+\.\d*'

try:
    filename = sys.argv[1]
except IndexError:
    exit('Bad file name')


with open(filename,'r') as fdat:
    data = fdat.read()

eta = re.findall('\s*eta\s+=\s*('+scinot+')', data)
eta = list(map(float, eta))

dat = re.findall('\s+('+scinot+')\s+('+flonot+r')\n', data)


temp = [float(x[1]) for x in dat]
#temp = sorted(list(set(temp)))

rhoyl = [float(x[0]) for x in dat]

fig, ax = plt.subplots(figsize=(8,6))

ax.plot(rhoyl, temp)

plt.show()
