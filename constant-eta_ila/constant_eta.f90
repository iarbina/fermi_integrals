program constant_eta
  use, intrinsic :: iso_fortran_env, wp=>real64
  implicit none
  integer :: i, j
  integer, parameter :: ntemp = 27
  real(wp) :: temp_min, temp_max, mlep, f12, f32, fac, etap, Fm, Fp
  real(wp), allocatable, dimension(:) :: eta, tarray, rhoylarray, beta
  real(wp), parameter :: pi = 3.14159265358979323846d0
  real(wp), parameter :: meMeV = 0.510998910_wp  ! MeV
  real(wp), parameter :: mmuMeV = 105.6583668_wp ! MeV
  real(wp), parameter :: hbarc =  1.973269804d-11 ! MeV cm
  real(wp), parameter :: hbarcfm = 197.3269788d0 ! MeV fm
  real(wp), parameter :: mu = 1.66053906660d-24 ! g
  real(wp), parameter :: kb = 8.617333262d-11 ! MeV / K 
  real(wp), external :: gfd_d3
  
  mlep = memev
  !mlep = mmumev
  

  eta = [-20._wp, -15._wp, -10._wp,-3._wp,-1._wp,0._wp,    &
         3._wp, 10._wp, 20._wp, 50._wp, 500._wp, 9000._wp, &
         1.e5_wp, 1.e6_wp, 1.e7_wp, 1.e8_wp, 1.e9_wp,      &
         1.e10_wp, 1.e11_wp, 1.e12_wp]

  temp_min = 1.e-1_wp; temp_max = 1.e12_wp
  tarray = [ (temp_min*10.0_wp**((i-1)*(log10(temp_max)-log10(temp_min))/(ntemp-1)), &
              i=1, ntemp) ]

  allocate(beta(size(tarray)), rhoylarray(size(tarray)))
  beta = kb * tarray / mlep

  eta_loop: do j=1, size(eta)

    write(*,'("eta = ",es14.6e3)') eta(j)

    temp_loop: do i=1, size(beta)

      fac = mlep*sqrt(beta(i))/hbarc
      fac = sqrt(2.0d0)*fac*fac*fac/(pi*pi) * mu
      
      f12 = gfd_d3 (0, 0.5d0, eta(j), beta(i))
      f32 = gfd_d3 (0, 1.5d0, eta(j), beta(i))
      Fm  = f12 + beta(i) * f32

      etap = - eta(j) - 2.0_wp / beta(i)
      f12 = gfd_d3 (0, 0.5d0, etap, beta(i))
      f32 = gfd_d3 (0, 1.5d0, etap, beta(i))
      Fp  = f12 + beta(i) * f32

      rhoylarray(i) = fac * (Fm - Fp)

      write(*,'(1x,es14.6,1x,f5.2)') rhoylarray(i), log10(tarray(i))

    end do temp_loop

  end do eta_loop
  

end program constant_eta
