module fermi_series

! Includes Fermi-Dirac integrals in terms of power series
! in beta for small degeneracy parameter fulfilling the 
! conditions:
!
!       eta > -30   and   log10(beta) < -1
!
! Formulas taken from Eqs. (C.15), (C.16) and (C.17)
! from:
!     Z. Gong, et al. Comp. Phys. Comm., 136, 3, (2001)
  
  use, intrinsic :: iso_fortran_env, wp => real64
  implicit none
  real(wp), parameter, private :: pi = 3.14159265358979311599796346854_wp
  real(wp), parameter, private :: sqrtpi = sqrt(pi)
  real(wp), parameter :: hbarc = 197.3269804_wp

contains

  elemental function fermi_polylog(k,val) result(res)
    integer,  intent(in) :: k
    real(wp), intent(in) :: val
    real(wp) :: res
    res = - gamma(real(k+1,wp)) * polylog(k+1,-exp(val))
  end function fermi_polylog

  elemental function polylog(k,val) result(res)
    integer,  intent(in) :: k
    real(wp), intent(in) :: val
    real(wp) :: res, aux, x, a
    real(wp), parameter :: eps = 1.e-16_wp
    integer :: n
    if (abs(val) >= 1._wp) then
      res = 0._wp
    else
      n   = 0
      x   = 1._wp
      res = 0._wp
      do
        a   =  1._wp / (real(n,wp) + 1._wp)**real(k,wp)
        aux = a * x
        res = res + aux
        !print'(i5,*(3x,es23.15e3))', n, res*val, aux*val
        if (abs(aux) < eps*res) exit
        n = n + 1
        x = x * val
      end do
      res = res * val
    end if
  end function polylog
  
  function fermi_smalldeg_one_half(eta, beta) result(res)
    real(wp) :: eta, beta, res
    if (check_params(eta, beta) /= 0) return
    res = 0.5_wp * sqrtpi * exp(eta)        &
        * (1._wp + 3._wp/8._wp * beta       &
        -  15._wp/128._wp * beta*beta       &
        +  105._wp/1024._wp * beta*beta*beta * (1._wp - beta)) 
  end function fermi_smalldeg_one_half
  
  function fermi_smalldeg_three_halves(eta, beta) result(res)
    real(wp) :: eta, beta, res
    if (check_params(eta, beta) /= 0) return
    res = 0.75_wp * sqrtpi * exp(eta)         &
        * (1._wp + 5._wp/8._wp * beta         &
        -  35._wp/128._wp * beta*beta         &
        -  2345._wp/16384._wp * beta*beta*beta) 
  end function fermi_smalldeg_three_halves
  
  function fermi_smalldeg_five_halves(eta, beta) result(res)
    real(wp) :: eta, beta, res
    if (check_params(eta, beta) /= 0) return
    res = 0.5_wp * sqrtpi * exp(eta)     &
        * (1._wp + 7._wp/8._wp * beta    &
        -  539._wp/4090._wp * beta*beta)
  end function fermi_smalldeg_five_halves

  function check_params (eta, beta) result(istat)
    real(wp) :: eta, beta
    integer  :: istat
    if (eta < -30._wp .or. log10(beta) > -1) then
       istat = 1
       write(error_unit,'("WARNING: fermi series conditions not fulfilled")')
     else
       istat = 0
    end if
  end function check_params

end module fermi_series

program main

  use, intrinsic :: iso_fortran_env, wp => real64
  use fermi_series
  
  implicit none
  integer, parameter :: ndim = 1000
  real(wp) :: eta, beta, gs, temp
  real(wp) :: gi, gf, grid(ndim), plog(ndim), fplog(ndim),nden(ndim), time(2)
  integer  :: k, i

  gi = -1.e2_wp
  gf = 0._wp
  grid = [(gi+(i-1)*(gf-gi)/(ndim-1), i=1, ndim)]
  call cpu_time(time(1))
  plog = polylog(3,-exp(grid))
  call cpu_time(time(2))
  print'(":: cpu time polylog = ",es23.15e3)', time(2)-time(1)
  call cpu_time(time(1))
  fplog = fermi_polylog(2,grid)
  call cpu_time(time(2))
  print'(":: cpu time fermi polylog = ",es23.15e3)', time(2)-time(1)
  gs = 1._wp
  temp = 10._wp
  nden = gs*0.5_wp / (pi*pi) * (temp/hbarc)**3 * fplog
  do i=1, ndim-1
    write(1234,'(*(3x,es23.15e3))') grid(i), plog(i), fplog(i), nden(i)
  end do
  !do
  !  print'(":: Enter k and eta for Li_k(-e^eta): ")'
  !  read(input_unit,*) k, eta

  !  print'(":: Li_",i1,"(",es23.15e3,") = ",es23.15e3)', k, -exp(eta), polylog(k,-exp(eta))
  !end do

  !do
  !  print'(":: Enter eta and beta: ")'
  !  read(input_unit,*) eta, beta

  !  print'(":: F_1/2(eta,beta) = ",g0)', fermi_smalldeg_one_half(eta, beta) 
  !  print'(":: F_3/2(eta,beta) = ",g0)', fermi_smalldeg_three_halves(eta, beta) 
  !  print'(":: F_5/2(eta,beta) = ",g0)', fermi_smalldeg_five_halves(eta, beta) 
  !end do
  

end program main
