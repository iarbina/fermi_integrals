!=======================================================================
!
!     BOLTZTRAN: FERMI_2
!
!=======================================================================

      subroutine FERMI_2(y,FDUM2) 

      use boltztran_memory_module, only: jp,kp,nesp,pie,CH20,CH2,
     1  expmax,jmax,kmax,nquad

      implicit none

      real, intent(in) :: y
      real, intent(out) :: FDUM2

!-----------------------------------------------------------------------

      integer :: i,j,k,l,m,n
      real :: xd,fexp,x1,x2,x2inv,y1,F2
      real :: T0,T1,T2,T3,T4,T5,T6,T7,T8,T9
      real :: T10,T11,T12,T13,T14,T15,T16,T17,T18,T19
      real :: T20,T21,T22,T23,T24,T25,T26

      fexp(xd)=exp( min( expmax, max( -expmax, xd ) ) ) 


!$OMP PARALLEL DO default(private) shared(pie,
!$OMP1 CH20,CH2,expmax,jmax,kmax,nquad,y,FDUM2)

      x1=y
      x2=-fexp(x1)
      x2inv=1.0/x2

      if(x2.lt.-1.0) x2=x2inv 
      y1=(4.0*x2+1.0)/3.0

      T0=1.0
      T1=y1
      T2=2.0*y1*T1-T0
      T3=2.0*y1*T2-T1
      T4=2.0*y1*T3-T2
      T5=2.0*y1*T4-T3
      T6=2.0*y1*T5-T4
      T7=2.0*y1*T6-T5
      T8=2.0*y1*T7-T6
      T9=2.0*y1*T8-T7
      T10=2.0*y1*T9-T8
      T11=2.0*y1*T10-T9
      T12=2.0*y1*T11-T10
      T13=2.0*y1*T12-T11
      T14=2.0*y1*T13-T12
      T15=2.0*y1*T14-T13
      T16=2.0*y1*T15-T14
      T17=2.0*y1*T16-T15
      T18=2.0*y1*T17-T16
      T19=2.0*y1*T18-T17
      T20=2.0*y1*T19-T18
      T21=2.0*y1*T20-T19
      T22=2.0*y1*T21-T20
      T23=2.0*y1*T22-T21

      F2=-2.0*x2*(0.5*CH20*T0+CH2(1)*T1+CH2(2)*T2+CH2(3)*T3+CH2(4)*T4
     c                         +CH2(5)*T5+CH2(6)*T6+CH2(7)*T7+CH2(8)*T8
     c                         +CH2(9)*T9+CH2(10)*T10+CH2(11)*T11
     c                         +CH2(12)*T12+CH2(13)*T13+CH2(14)*T14
     c                         +CH2(15)*T15+CH2(16)*T16+CH2(17)*T17
     c                         +CH2(18)*T18+CH2(19)*T19+CH2(20)*T20
     c                         +CH2(21)*T21+CH2(22)*T22+CH2(23)*T23
     c           )                                                                  
      if((1.0/x2inv).lt.-1.0) F2=F2+pie**2*x1/3.0+x1**3/3.0
      FDUM2=F2

   1  continue 
!$OMP END PARALLEL DO


      return
      end 

!=======================================================================
