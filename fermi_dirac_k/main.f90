program main
  use,intrinsic::iso_fortran_env, wp=>real64
  use constants
  implicit none
  real(wp), allocatable :: mue(:), eta(:)
  real(wp) :: mue_min, mue_max, dmue
  real(wp) :: eta_min, eta_max, deta
  real(wp) :: dk, beta, fd, fdeta, fdbeta, temp
  integer  :: i, np
  integer, parameter :: iounit = 999

  print'(":: Fermi-Dirac integrals calculator")'
  print'(":: Enter order and temperature:")'
  read(input_unit,*) dk, temp
  print'(">> dk: ",g0)', dk


  np = 10000
  mue_min = -1.0e5_wp
  mue_max = 1.0e5_wp

  dmue = (mue_max - mue_min) / (np - 1)

  mue = [ (mue_min + (i-1) * dmue, i=1, np) ]

  !allocate( eta(size(mue)) )
  !eta = (mue - memev) / temp
  
  eta_min = -1.0e3_wp
  eta_max = 1.0e3_wp
  deta = (eta_max - eta_min) / (np - 1)
  eta = [ (eta_min + (i-1) * deta, i=1, np) ]

  beta = temp / memev
  !beta = 0.0_wp

  do i=1, np
     call dfermi (dk, eta(i), beta, fd, fdeta, fdbeta)
     write(iounit,*) mue(i), eta(i), fd 
  end do

end program main
