program main_ferm_int
  use,intrinsic::iso_fortran_env,wp=>real64
  use fermi_integrals_mod
  implicit none
  integer, parameter :: n = 200
  real(wp) :: x(n)
  integer :: i

  x = [(1.e100_wp*(i-1)/(n-1), i=1, n)]

  do i=1, n
    print'(*(4x,es14.5e3))', x(i), inverse_fermi_one_half(x(i))
  end do
  print'(*(4x,es24.16e3))', huge(x(1)), inverse_fermi_one_half(huge(x(1))), 1._wp/huge(x(1)), tiny(x(1))
end program main_ferm_int
